﻿namespace GrimbergJason_ContactList
{
    public class Contacts
    {
        // Contact Variables
        string firstName;
        string lastName;
        string phoneNumber;
        string contactEmail;
        int imageIndex;

        // Encapsulate all variables
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string ContactEmail { get => contactEmail; set => contactEmail = value; }
        public int ImageIndex { get => imageIndex; set => imageIndex = value; }

        // Override all strings to display in list
        public override string ToString()
        {
            return FirstName.ToString().Trim() + " " + LastName.ToString().Trim() +
                " " + PhoneNumber.ToString().Trim() + " " + ContactEmail.ToString().Trim();
        }
    }
}
