﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace GrimbergJason_ContactList
{
    public partial class MainForm : Form
    {
        // --------------------------------------------------------------------------------------------------
        // EventHandler call
        // --------------------------------------------------------------------------------------------------
        public EventHandler ObjectAdded;
        public EventHandler SelectionChanged;
        public EventHandler<ModifyObjectEventArgs> ModifyObject;
        // --------------------------------------------------------------------------------------------------
        // Classes
        // --------------------------------------------------------------------------------------------------
        public class ModifyObjectEventArgs : EventArgs
        {
            // Implement the ListViewItem to the modified object
            ListViewItem ObjectToModify;

            // Getters and setters for the ListViewItem
            public ListViewItem ObjectToModify1
            {
                // Get the object to modify
                get
                {
                    return ObjectToModify;
                }

                // Set the object to modify (selected item in list-view)
                set
                {
                    ObjectToModify = value;
                }
            }

            // Event argument for the modified object
            public ModifyObjectEventArgs(ListViewItem lvi)
            {
                ObjectToModify = lvi;
            }
        }
        
        // Selected object check
        public Contacts SelectedObject
        {
            // Get the selected object
            get
            {
                // Fill in the data field with the selected item
                if (lvAllContacts.SelectedItems.Count > 0)
                {
                    // Show the remove button
                    btnRemove.Visible = true;
                    
                    // Return the object Tag
                    return lvAllContacts.SelectedItems[0].Tag as Contacts;
                }
                else
                {
                    // Hide the remove button
                    btnRemove.Visible = false;
                    
                    // Return nothing if nothing is selected
                    return new Contacts();
                }
            }
        }

        public MainForm()
        {
            InitializeComponent();
            
            //Check the large icon box
            menuLargeIcons.Enabled = false;
            menuLargeIcons.Checked = true;
        }

        // --------------------------------------------------------------------------------------------------
        // Triggers
        // --------------------------------------------------------------------------------------------------

        // Main form load
        public void MainForm_Load(object sender, EventArgs e)
        {
            UserInputDialog uid = new UserInputDialog();
            
            // Sub to the event handler for the object added
            uid.ObjectAdded = new EventHandler(this.ObjectAddedHandler);

            // Sub to the object added handler
            ObjectAdded += ObjectAddedHandler;

            // Sub to the selection changed handler
            SelectionChanged += uid.SelectionChangedHandler;
        }
        
        // Large icon menu item click
        private void menuLargeIcons_Click(object sender, EventArgs e)
        {
            // Enable the small icon label and 
            // un-check the box
            menuSmallIcons.Enabled = true;
            menuSmallIcons.Checked = false;

            // Disable the large icon label and 
            // check the box
            menuLargeIcons.Enabled = false;
            menuLargeIcons.Checked = true;

            // Call icon size method
            IconSize();
        }

        // Small icon menu item click
        private void menuSmallIcons_Click(object sender, EventArgs e)
        {
            // Enable the large icon label and 
            // un-check the box
            menuLargeIcons.Enabled = true;
            menuLargeIcons.Checked = false;

            // Disable the large icon label and 
            // check the box
            menuSmallIcons.Enabled = false;
            menuSmallIcons.Checked = true;

            // Call icon size method
            IconSize();
        }
        
        // Menu Exit item
        private void menuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Save contact info
        private void menuSave_Click(object sender, EventArgs e)
        {
            // Verify that none of the text boxes are blank
            if (lvAllContacts.Items.Count > 0)
            {
                // Call save file method
                SaveFile();
            }
            else
            {
                // Tell the user what is going on
                MessageBox.Show("Please have at least one(1) contact in your list.");
            }
        }

        // Remove button trigger
        private void btnRemove_Click(object sender, EventArgs e)
        {
            // Check to make sure something is selected
            if (lvAllContacts.SelectedItems.Count > 0)
            {
                // Deletes which ever item is selected in the list 
                foreach (ListViewItem i in lvAllContacts.SelectedItems)
                {
                    lvAllContacts.Items.Remove(i);
                }
            }
        }

        // --------------------------------------------------------------------------------------------------
        // Object Handlers
        // --------------------------------------------------------------------------------------------------
        public void ObjectAddedHandler(object sender, EventArgs e)
        {
            // Set the input form as the sender
            UserInputDialog uid = sender as UserInputDialog;
            // Set the items values to new data
            Contacts c = uid.Data;
            // Set a new instance of the list view item
            ListViewItem lvi = new ListViewItem();

            // Set the list view item as string
            lvi.Text = c.ToString();
            // Set the image index
            lvi.ImageIndex = c.ImageIndex;
            // Set the new list view item tag as the string
            lvi.Tag = c;

            // Add the text boxes to the list
            lvAllContacts.Items.Add(lvi);
        }
        
        // --------------------------------------------------------------------------------------------------
        // Methods
        // --------------------------------------------------------------------------------------------------

        // Modify object event
        public void ModifyObjectEvent()
        {
            // Once the user verified they would like to proceed, modify selected object
            if (ModifyObject != null && lvAllContacts.SelectedItems.Count > 0)
            {
                // Modify the current selected object to the new inputs
                ModifyObject(this, new ModifyObjectEventArgs(lvAllContacts.SelectedItems[0]));
            }
            else
            {
                // Show an error if there is not a selected item to update
                MessageBox.Show("Please highlight something to update.", "Oops", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // --------------------------------------------------------------------------------------------------
        // Save file method
        // --------------------------------------------------------------------------------------------------
        public void SaveFile()
        {
            // Use the SaveFileDialog to open a save dialog box
            using (var sfd = new SaveFileDialog())
            {
                // Set the file name to a variable
                string filename = "";

                // Show only text files to write out to
                sfd.Filter = "Text files (*.txt)|*.txt";

                // Change the title of the save dialog 
                sfd.Title = "Save a Text File";

                // Show the dialog for save
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    // Change the filename variable to the file
                    filename = sfd.FileName.ToString();

                    if (filename != "")
                    {
                        // Start using the stream writer with the file name of the user's choosing
                        using (StreamWriter sw = new StreamWriter(filename))
                        {
                            // Add a header to the beginning to the text file
                            sw.WriteLine(string.Format("Contact List:\r\n" +
                                                       "***************"));

                            // Loop through the list to save out 
                            foreach (ListViewItem item in lvAllContacts.Items)
                            {
                                // Write the list out to the text file
                                sw.WriteLine(item.SubItems[0].Text);
                            }
                        }
                    }
                }
            }
        }

        // ---------------- //
        // Icon size method //
        // ---------------- //
        public void IconSize()
        {
            // Change the icons
            if (menuLargeIcons.Checked == true)
            {
                // Change to large icons
                lvAllContacts.View = View.LargeIcon;
            }
            else if (menuSmallIcons.Checked == true)
            {
                // Change to small icons
                lvAllContacts.View = View.SmallIcon;
            }
        }

        // --------------------- //
        // Is Valid Email method //
        // --------------------- //
        bool IsValidEmail(string email)
        {
            // Try to gather the email
            try
            {
                // Set the email string to a variable
                // and use the built in email checker
                var addr = new System.Net.Mail.MailAddress(email);
                // If it is valid it will set the email to true
                return addr.Address == email;
            }
            // Catch the error
            catch
            {
                // Return if this is not a valid email
                return false;
            }
        }

        private void lvAllContacts_DoubleClick(object sender, EventArgs e)
        {
            // New instance of the user input form
            UserInputDialog uid = new UserInputDialog();

            // Set the selection to the new window
            SelectionChanged = new EventHandler(uid.SelectionChangedHandler);

            // Sub to the modified handle event
            ModifyObject += uid.HandleModifyObject;

            // Invoke the selection to call the new data coming into the new window
            SelectionChanged?.Invoke(this, new EventArgs());

            // Send the update button to true
            uid.UpdateButton(true);

            // Show new window to add a contact
            uid.ShowDialog();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // New instance of the user input window
            UserInputDialog uid = new UserInputDialog();

            // Sub to the event handler for the object added
            uid.ObjectAdded = new EventHandler(this.ObjectAddedHandler);

            // Show a new user input window
            uid.ShowDialog();
        }
    }
}
