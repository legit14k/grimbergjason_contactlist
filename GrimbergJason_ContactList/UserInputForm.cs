﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrimbergJason_ContactList
{
    public partial class UserInputDialog : Form
    {
        // Creating a new instance of the main window
        MainForm main = new MainForm();

        // --------------------------------------------------------------------------------------------------
        // EventHandler call
        // --------------------------------------------------------------------------------------------------
        public EventHandler ObjectAdded;

        // --------------------------------------------------------------------------------------------------
        // Contact data components
        // --------------------------------------------------------------------------------------------------
        // New contacts data
        public Contacts Data
        {
            // Get all the contact data
            get
            {
                // Capture all of the text boxes
                Contacts c = new Contacts();
                c.FirstName = txtFirstName.Text;
                c.LastName = txtLastName.Text;
                c.PhoneNumber = txtPhoneNumber.Text;
                c.ContactEmail = txtEmail.Text;
                c.ImageIndex = 0;
                return c;
            }

            // Set all the contact data
            set
            {
                // Set all text boxes
                txtFirstName.Text = value.FirstName;
                txtLastName.Text = value.LastName;
                txtPhoneNumber.Text = value.PhoneNumber;
                txtEmail.Text = value.ContactEmail;
            }
        }

        // Selected handler
        public void SelectionChangedHandler(object sender, EventArgs e)
        {
            // Setting the main window as the sender
            main = sender as MainForm;

            // Throwing the values that the list view has stored
            Data = main.SelectedObject;
        }

        public UserInputDialog()
        {
            InitializeComponent();
            
            // Hide the update button
            btnUpdate.Visible = false;
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Creating a new instance of the main window
            main = new MainForm();

            // Subscribing to the new added handler
            ObjectAdded += main.ObjectAddedHandler;

            // Call add method
            AddToList();

            // Clear data in the input fields
            Data = new Contacts();
        }

        // --------------- //
        // Add item method //
        // --------------- //
        public void AddToList()
        {
            // Verify that none of the text boxes are blank
            if (txtFirstName.Text != "" || txtLastName.Text != "" ||
                txtPhoneNumber.Text != "" || txtEmail.Text != "")
            {
                // Verify first name
                if (txtFirstName.Text.Any(char.IsDigit))
                {
                    MessageBox.Show("Please do not have numbers in first name.");
                    return;
                }

                // Verify last name
                if (txtLastName.Text.Any(char.IsDigit))
                {
                    MessageBox.Show("Please do not have numbers in last name.");
                    return;
                }

                // Verify only numbers in phone number
                Int64 parsedValue;
                if (!Int64.TryParse(txtPhoneNumber.Text, out parsedValue))
                {
                    MessageBox.Show("Please enter only numbers for phone number.");
                    return;
                }

                // Validate email
                // Set the value to variable
                bool valEmail = IsValidEmail(txtEmail.Text);
                // True will add the object to list
                if (valEmail == true)
                {
                    // Make sure the object is not null
                    if (ObjectAdded != null)
                    {
                        ObjectAdded(this, new EventArgs());
                    }
                }
                // False will show error and what went wrong
                else if (valEmail == false)
                {
                    MessageBox.Show("Not a valid email.");
                }

                // Clearing the input fields and hiding the window
                Data = new Contacts();
                this.Close();
            }
        }

        // --------------------- //
        // Is Valid Email method //
        // --------------------- //
        bool IsValidEmail(string email)
        {
            // Try to gather the email
            try
            {
                // Set the email string to a variable
                // and use the built in email checker
                var addr = new System.Net.Mail.MailAddress(email);
                // If it is valid it will set the email to true
                return addr.Address == email;
            }
            // Catch the error
            catch
            {
                // Return if this is not a valid email
                return false;
            }
        }

        // Update button method
        public void UpdateButton(bool i)
        {
            // Change the apply button to what is entered
            btnUpdate.Visible = i;
        }

        // Handler for the modified object
        public void HandleModifyObject(object sender, MainForm.ModifyObjectEventArgs e)
        {
            // Set the new values as the tag for Contacts
            Contacts c = e.ObjectToModify1.Tag as Contacts;
            c.FirstName = txtFirstName.Text;
            c.LastName = txtLastName.Text;
            c.PhoneNumber = txtPhoneNumber.Text;
            c.ContactEmail = txtEmail.Text;

            // Set the new values as the new object
            e.ObjectToModify1.Text = c.ToString();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            // Show dialog box to tell the user what is going on
            DialogResult dialogResult = MessageBox.Show("You are about to overwrite the current selected item.", "Are You Sure?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                // Once the user verified they would like to proceed, modify selected object
                if (txtFirstName.Text != "" || txtLastName.Text != "" ||
                        txtPhoneNumber.Text != "" || txtEmail.Text != "")
                {
                    // Call to the modified object change
                    main.ModifyObject = new EventHandler<MainForm.ModifyObjectEventArgs>(this.HandleModifyObject);

                    // Verify that none of the text boxes are blank
                    if (txtFirstName.Text != "" || txtLastName.Text != "" ||
                        txtPhoneNumber.Text != "" || txtEmail.Text != "")
                    {
                        // Verify first name
                        if (txtFirstName.Text.Any(char.IsDigit))
                        {
                            MessageBox.Show("Please do not have numbers in first name.");
                            return;
                        }

                        // Verify last name
                        if (txtLastName.Text.Any(char.IsDigit))
                        {
                            MessageBox.Show("Please do not have numbers in last name.");
                            return;
                        }

                        // Verify only numbers in phone number
                        Int64 parsedValue;
                        if (!Int64.TryParse(txtPhoneNumber.Text, out parsedValue))
                        {
                            MessageBox.Show("Please enter only numbers for phone number.");
                            return;
                        }

                        // Validate email
                        // Set the value to variable
                        bool valEmail = IsValidEmail(txtEmail.Text);

                        // True will add the object to list
                        if (valEmail == true)
                        {
                            // invoke the modified object event
                            main.ModifyObjectEvent();
                        }
                        // False will show error and what went wrong
                        else if (valEmail == false)
                        {
                            MessageBox.Show("Not a valid email.");
                        }
                    }

                    // Tell the user that the update was successful
                    MessageBox.Show("Updated!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
                else
                {
                    // Show an error if there is not a selected item to update
                    MessageBox.Show("Please highlight something to update.", "Oops", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if(dialogResult == DialogResult.No)
            {
                this.Close();
                return;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            // Clearing the input fields and hiding the window
            Data = new Contacts();
            this.Close();
        }
    }
}
